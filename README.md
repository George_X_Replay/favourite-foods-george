# README #

Here is the simple class repository we talked about in the lecture. 

It is about your favourite food.

* Clone or fork this repository ( you may want to make yours private - although if you do make it private, I need access to it in order to resolve conflicts. Perhaps keep it public initially )
* Add a page for your favourite food (I started by adding pasta.html)
* Hyperlink the page to index.html
* Create a pull request

Just plain html, no images and fancy layouts please. Just a quick title, a description and perhaps a link to a recipe.